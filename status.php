<?php

$data = getFileData();

/**
 * Method getFileData to get data from status file
 * 
 * @author Indrajeet singh <indrajeet.singh@kelltontech.com>
 * @return  array
 */
function getFileData() {
    //$statusFile = "/usr/local/nagios/var/status.dat";
    $statusFile = 'status.dat';
    $nag_version = getFileVersion($statusFile); // returns integer 2 or 3
    $created_ts = 0;
    $debug = false;

    if ($nag_version == 2) {
        $data = getData2($statusFile); // returns an array
    } else {
        $data = getData($statusFile); // returns an array
    }
    return $data;
}

/**
 * Method getFileVersion to check status log file version
 * 
 * @param string $statusFile Status log file name
 *
 * @author Indrajeet singh <indrajeet.singh@kelltontech.com>
 * @return  int
 */
function getFileVersion($statusFile) {
    global $created_ts;
    $version = 2;

    $fh = fopen($statusFile, 'r');
    $inInfo = false;
    while ($line = fgets($fh)) {
        if (trim($line) == "info {") {
            $inInfo = true;
        } elseif (trim($line) == "}") {
            $inInfo = false;
            break;
        } elseif ($inInfo) {
            $vals = explode("=", $line);
            if (trim($vals[0]) == "created") {
                $created = $vals[1];
            } elseif (trim($vals[0]) == "version") {
                $version = $vals[1];
            }
        }
    }
    return $version;
}

/**
 * Method getData2 to read status log file data
 * 
 * @param string $statusFile Status log file name
 *
 * @author Indrajeet singh <indrajeet.singh@kelltontech.com>
 * @return  array
 */
function getData2($statusFile) {
    // the keys to get from host status:
    $host_keys = array('host_name', 'has_been_checked', 'check_execution_time', 'check_latency', 'check_type', 'current_state', 'current_attempt', 'state_type', 'last_state_change', 'last_time_up', 'last_time_down', 'last_time_unreachable', 'last_notification', 'next_notification', 'no_more_notifications', 'current_notification_number', 'notifications_enabled', 'problem_has_been_acknowledged', 'acknowledgement_type', 'active_checks_enabled', 'passive_checks_enabled', 'last_update');

    // keys to get from service status:
    $service_keys = array('host_name', 'service_description', 'has_been_checked', 'check_execution_time', 'check_latency', 'current_state', 'state_type', 'last_state_change', 'last_time_ok', 'last_time_warning', 'last_time_unknown', 'last_time_critical', 'plugin_output', 'last_check', 'notifications_enabled', 'active_checks_enabled', 'passive_checks_enabled', 'problem_has_been_acknowledged', 'acknowledgement_type', 'last_update', 'is_flapping');

    // open the file
    $fh = fopen($statusFile, 'r');

    // variables to keep state
    $inSection = false;
    $sectionType = "";
    $lineNum = 0;
    $sectionData = array();

    $hostStatus = array();
    $serviceStatus = array();

    //variables for total hosts and services
    $typeTotals = array();

    // loop through the file
    while ($line = fgets($fh)) {
        $lineNum++; // increment counter of line number, mainly for debugging
        $line = trim($line); // strip whitespace
        if ($line == "") {
            continue;
        } // ignore blank line
        if (substr($line, 0, 1) == "#") {
            continue;
        } // ignore comment
        // ok, now we need to deal with the sections

        if (!$inSection) {
            // we're not currently in a section, but are looking to start one
            if (strstr($line, " ") && (substr($line, -1) == "{")) { // space and ending with {, so it's a section header
                $sectionType = substr($line, 0, strpos($line, " ")); // first word on line is type
                $inSection = true;
                // we're now in a section
                $sectionData = array();

                // increment the counter for this sectionType
                if (isset($typeTotals[$sectionType])) {
                    $typeTotals[$sectionType] = $typeTotals[$sectionType] + 1;
                } else {
                    $typeTotals[$sectionType] = 1;
                }
            }
        }

        if ($inSection && $line == "}") { // closing a section
            if ($sectionType == "service") {
                $serviceStatus[$sectionData['host_name']][$sectionData['service_description']] = $sectionData;
            }
            if ($sectionType == "host") {
                $hostStatus[$sectionData["host_name"]] = $sectionData;
            }
            $inSection = false;
            $sectionType = "";
            continue;
        } else {
            // we're currently in a section, and this line is part of it
            $lineKey = substr($line, 0, strpos($line, "="));
            $lineVal = substr($line, strpos($line, "=") + 1);

            // add to the array as appropriate
            if ($sectionType == "service") {
                if (in_array($lineKey, $service_keys)) {
                    $sectionData[$lineKey] = $lineVal;
                }
            } elseif ($sectionType == "host") {
                if (in_array($lineKey, $host_keys)) {
                    $sectionData[$lineKey] = $lineVal;
                }
            }
            // else continue on, ignore this section, don't save anything
        }
    }

    fclose($fh);

    $retArray = array("hosts" => $hostStatus, "services" => $serviceStatus);

    return $retArray;
}

/**
 * Method getData to read status log file data
 * 
 * @param string $statusFile Status log file name
 *
 * @author Indrajeet singh <indrajeet.singh@kelltontech.com>
 * @return  array
 */
function getData($statusFile) {
    global $debug;

    // open the file
    $fh = fopen($statusFile, 'r');

    // variables to keep state
    $inSection = false;
    $sectionType = "";
    $lineNum = 0;
    $sectionData = array();

    $hostStatus = array();
    $serviceStatus = array();
    $programStatus = array();

    // variables for total hosts and services
    $typeTotals = array();

    // loop through the file
    while ($line = fgets($fh)) {
        $lineNum++; // increment counter of line number, mainly for debugging
        $line = trim($line); // strip whitespace
        if ($line == "") {
            continue;
        } // ignore blank line
        if (substr($line, 0, 1) == "#") {
            continue;
        } // ignore comment
        // ok, now we need to deal with the sections
        if (!$inSection) {
            // we're not currently in a section, but are looking to start one
            if (substr($line, strlen($line) - 1, 1) == "{") { // space and ending with {, so it's a section header
                $sectionType = substr($line, 0, strpos($line, " ")); // first word on line is type
                $inSection = true;
                // we're now in a section
                $sectionData = array();

                // increment the counter for this sectionType
                if (isset($typeTotals[$sectionType])) {
                    $typeTotals[$sectionType] = $typeTotals[$sectionType] + 1;
                } else {
                    $typeTotals[$sectionType] = 1;
                }
            }
        } elseif ($inSection && trim($line) == "}") { // closing a section
            if ($sectionType == "servicestatus") {
                $serviceStatus[$sectionData['host_name']][$sectionData['service_description']] = $sectionData;
            } elseif ($sectionType == "hoststatus") {
                $hostStatus[$sectionData["host_name"]] = $sectionData;
            } elseif ($sectionType == "programstatus") {
                $programStatus = $sectionData;
            }
            $inSection = false;
            $sectionType = "";
            continue;
        } else {
            // we're currently in a section, and this line is part of it
            $lineKey = substr($line, 0, strpos($line, "="));
            $lineVal = substr($line, strpos($line, "=") + 1);

            // add to the array as appropriate
            if ($sectionType == "servicestatus" || $sectionType == "hoststatus" || $sectionType == "programstatus") {
                if ($debug) {
                    echo "LINE " . $lineNum . ": lineKey=" . $lineKey . "= lineVal=" . $lineVal . "=\n";
                }
                $sectionData[$lineKey] = $lineVal;
            }
            // else continue on, ignore this section, don't save anything
        }
    }

    fclose($fh);

    $retArray = array("hosts" => $hostStatus, "services" => $serviceStatus, "program" => $programStatus);
    return $retArray;
}
?>
