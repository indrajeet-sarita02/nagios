<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />        
        <meta http-equiv="refresh" content="60">
        <title>Server Monitoring Dashboard</title>
        <style>
            .service_container{ display:inline-block; width:100%; margin-bottom:20px;}
            .data_container{ float:left; width:48%;}
            .scrolls{ height:300px; overflow-x:hidden;}

            table{
                background:#FFFFFF;
                border-right: 1px solid #DFDFDF;
                border-top: 1px solid #DFDFDF;
                margin-bottom: 20px;
            }
            table th,table td{
                border-bottom: 1px solid #DFDFDF;
                border-left: 1px solid #DFDFDF; 
                padding: 10px ;
                font-size: 12px;
            }
            table th{
                font-weight: 700;
                vertical-align: middle;
            }
            table a{
                color: #008651; 
                text-decoration: underline;
            }
            table a:hover,table a:focus{
                text-decoration: none;
            }
            table td{
                text-align: center;
            }
            table td img{
                margin: 0 auto;
            }
            .data{
                background: #F1F1F0;
            }

            h2{ 
                font-size: 18px; 
                line-height: 120%;
                font-weight: 700;
                padding-bottom: 10px;
                color: #008651;
            }
            h3,.dataTitle{
                font-size: 15px; 
                line-height: 120%;
                font-weight: 700;
                padding: 5px;
                color: #000;
                background: #FFFFFF;
            }

            .service_display{display: none;}
            .view_all:hover .service_display{
                display: block;
                position:absolute;
                z-index: 999;
            }
            .service_display table{ color: #494949;} 
            .host_ok{background: #66FF66;}
            .host_critical{background: #FFBBBB;}
            .host_warning{background: #FFFF00;}
            .host_unknown{background: #FFDA9F;}
            .dataTitle{ text-align: center;}
            .service_display{ border-radius:8px; box-shadow: 0 0 10px #75826a; overflow: hidden; width:80%;}
            .data_container{ position: relative;}
            .service_container h3{ transition: background 1s linear 0s,border 1s linear 0s;-ms-transition: background 1s linear 0s,border 1s linear 0s;-moz-transition: background 1s linear 0s,border 1s linear 0s;-webkit-transition: background 1s linear 0s,border 1s linear 0s;}
        </style>
    </head>
    <body><?php
require_once('status.php');
$hosts = $data['hosts'];
$services = $data['services'];
$program = $data['program'];

function serviceAlert($status) {
    switch ($status) {
        case "0":
            return array('host_ok', 'OK');
            break;
        case "1":
            return array('host_warning', 'WARNING');
            break;
        case "2":
            return array('host_critical', 'CRITICAL');
            break;
        case "3":
            return array('host_unknown', 'UNKNOWN');
            break;
    }
}
?>
        <div id="main-container">
            <div class="service_container">
                <div class="data_container" id="server_health">
                    <h3> Server Status</h3>
                    <div class="scrolls">
                        <table width="100%" cellspacing="0" cellpadding="0" >
                            <tr>
                                <th>Host</th>
                                <th>Service</th>
                                <th>Information</th>
                                <th>Status</th>
                                <th>View Option</th>
                            </tr>
                            <?php
                            $oldHost = '';
                            $serverError = '';
                            foreach ($services as $service) {
                                foreach ($service as $key => $serv) {
                                    $currentState = serviceAlert($serv['current_state']);
                                    ?>
                                    <tr class="data">
                                        <td><?php
                            if ($serv['host_name'] != $oldHost) {
                                $oldHost = $serv['host_name'];
                                echo $serv['host_name'];
                            }
                                    ?></td>
                                        <td><?php echo $key; ?></td>
                                        <td><?php echo $serv['plugin_output']; ?>
                                        <td class="<?php echo $currentState[0]; ?>"><?php echo $currentState[1]; ?></td>
                                        <td><a href="javascript: void(0);" class="view_all">View
                                                <div class="service_display">
                                                    <div class="dataTitle">Service State Information</div>
                                                    <table width="100%" cellspacing="0" cellpadding="0" >
                                                        <tbody><tr><td>Status:</td><td class="<?php echo $currentState[0]; ?>"><?php echo $currentState[1]; ?></td></tr>
                                                            <tr><td valign="top">Status Information:</td><td><?php echo $serv['plugin_output']; ?></td></tr>
                                                            <tr><td valign="top">Performance Data:</td><td><?php echo $serv['performance_data']; ?></td></tr>
                                                            <tr><td>Last Check Time:</td><td><?php echo date("Y-m-d H:i:s", $serv['last_check']); ?></td></tr>
                                                            <tr><td>Check Latency / Duration:</td><td><?php echo $serv['check_latency']; ?>&nbsp;/&nbsp;<?php echo $serv['check_execution_time']; ?></td></tr>
                                                            <tr><td>Next Scheduled Check:&nbsp;&nbsp;</td><td><?php echo date("Y-m-d H:i:s", $serv['next_check']); ?></td></tr>
                                                            <tr><td>Last State Change:</td><td><?php echo date("Y-m-d H:i:s", $serv['last_state_change']); ?></td></tr>
                                                            <tr><td>Last Update:</td><td><?php echo date("Y-m-d H:i:s", $serv['last_update']); ?></td></tr>
                                                        </tbody></table>
                                                </div>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </table>
                    </div>
                </div>

            </div>

        </div>  
    </body>
</html>
